package com.mkyong.rmiinterface;

import com.mkyong.rmiclient.A;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RMIInterface extends Remote {
	public String helloTo(String name, A a) throws RemoteException;
}
package com.mkyong.rmiclient;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import javax.swing.JOptionPane;

import com.mkyong.rmiinterface.RMIInterface;

public class ClientOperation {
	private static RMIInterface look_up;

	public static void main(String[] args) throws MalformedURLException, RemoteException, NotBoundException {
		
		look_up = (RMIInterface) Naming.lookup("//localhost/MyServer");
		String txt = JOptionPane.showInputDialog("What is your name?");
		A a = new A();
		a.setA("AAAAAAhhhhhh");
		B b = new B();
		b.setA("B's a");
		b.setB("BBBBBB");

		A ab = new B();
		ab.setA("B as A");

		String response = look_up.helloTo(txt, a);
		JOptionPane.showMessageDialog(null, response);

		String response2 = look_up.helloTo(txt, b);
		JOptionPane.showMessageDialog(null, response2);

		String response3 = look_up.helloTo(txt, ab);
		JOptionPane.showMessageDialog(null, response3);
	}
}

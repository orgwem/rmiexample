package com.mkyong.rmiclient;

import java.io.Serializable;

public class B extends A implements Serializable {

    private static final long serialVersionUID = 1L;

    private String b;

    public B() {
        super();
    }

    public String getB() {
        return b;
    }

    public void setB(String b) {
        this.b = b;
    }
}

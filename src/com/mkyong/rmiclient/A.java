package com.mkyong.rmiclient;

import java.io.Serializable;
import java.lang.String;

public class A  implements Serializable {

    private static final long serialVersionUID = 1L;

    private String a;

    public A() {
    }

    public String getA() {
        return a;
    }

    public void setA(String a) {
        this.a = a;
    }
}

# rmiExample

Kleines "Hello World" mit rmi nach dem Beispiel von <https://mkyong.com/java/java-rmi-hello-world-example/>, aber um Polymorphie ergänzt.

Zum Ausprobieren (am besten in drei separaten Fenstern):

1. rmiregistry starten

   `rmiregistry`

2. Server starten

   `cd <Ort wo die class files abgelegt sind>`
   
   `java com.mkyong.rmiserver.ServerOperation`

3. Client starten

   `cd <Ort wo die class files abgelegt sind>`
   
   `java com.mkyong.rmiclient.ClientOperation`
